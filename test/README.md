# Netguru Flutter Code Review

Create a repository.
Commit  and push the code from “flutter_rekru_cr_task-main.zip” to your main branch.
Copy “main_screen_list.diff” to your root directory for that project.
From that dir run “git apply main_screen_list.diff”.
Remove the “main_screen_list.diff” file.
Make a new feature branch from the main one.
From the same dir call “git add .” .
Commit and push those changes to your feature branch.
Make a pull request and start the code review.
When you finish, send us the repository with a code review to check.
